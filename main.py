import networkx as nx
from matplotlib import pyplot as plt
from numpy import transpose, linalg, array, sort, asarray, argmax, arange, matmul, argpartition, absolute, zeros
import numpy
from math import sqrt
import copy
import logging
import os
from memory_profiler import profile

logging.basicConfig(filename='log.log', level=logging.INFO, filemode="w")

def clear_pictures():
    for file in [file for file in os.listdir(os.getcwd()) if file.endswith(".png")]:
        os.remove(os.path.join(os.getcwd(), file))


# Example graph
nodes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
         30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
         58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85,
         86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
         111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132,
         133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149]
edges = [(0, 1), (0, 4), (0, 11), (0, 23), (0, 25), (0, 52), (0, 63), (0, 71), (0, 78), (0, 84), (0, 111), (0, 120),
         (0, 131), (1, 2), (1, 3), (1, 6), (1, 67), (1, 88), (1, 93), (2, 7), (2, 9), (2, 34), (2, 44), (2, 45),
         (2, 64), (2, 70), (3, 10), (3, 15), (3, 16), (3, 19), (3, 41), (3, 51), (3, 66), (3, 83), (3, 87), (3, 98),
         (3, 127), (4, 5), (4, 8), (4, 74), (4, 101), (4, 119), (5, 12), (5, 17), (5, 42), (5, 77), (5, 82), (7, 13),
         (7, 48), (7, 73), (8, 20), (8, 21), (8, 22), (8, 26), (8, 31), (8, 39), (8, 61), (8, 62), (10, 33), (11, 38),
         (11, 72), (11, 143), (12, 14), (12, 29), (12, 80), (13, 24), (13, 54), (13, 58), (13, 76), (13, 109),
         (13, 110), (13, 133), (14, 60), (14, 100), (15, 18), (15, 27), (16, 79), (16, 128), (17, 69), (20, 36),
         (20, 55), (20, 135), (21, 28), (21, 65), (21, 75), (21, 81), (21, 107), (21, 142), (22, 49), (22, 59),
         (22, 102), (22, 144), (22, 145), (23, 30), (24, 32), (24, 50), (24, 115), (24, 126), (28, 37), (31, 35),
         (31, 40), (31, 43), (31, 56), (31, 57), (31, 91), (31, 104), (31, 148), (34, 97), (35, 95), (35, 96),
         (36, 103), (36, 130), (39, 53), (40, 46), (40, 92), (40, 99), (45, 116), (45, 146), (46, 47), (46, 134),
         (47, 141), (48, 122), (52, 147), (53, 137), (53, 138), (57, 123), (60, 121), (61, 85), (62, 113), (63, 114),
         (65, 68), (65, 108), (65, 117), (65, 139), (69, 86), (69, 89), (69, 90), (69, 94), (73, 112), (78, 105),
         (81, 106), (86, 140), (87, 136), (91, 132), (97, 125), (111, 129), (116, 118), (116, 124), (128, 149)]


@profile
class first_try_graph(nx.Graph):
    def __init__(self, pregraph=None, random=True, nodes=45, prob=0.07, max_rank=100):
        super().__init__()

        if random == True:
            G = nx.erdos_renyi_graph(nodes, prob)
            # For testing
            G = nx.barabasi_albert_graph(nodes, prob)
            self.add_nodes_from(G.nodes)
            self.add_edges_from(G.edges)

            rng = numpy.random.default_rng()
            for i in self.nodes:
                for j in self.nodes:
                    try:
                        self[i][j]['weight'] = rng.integers(1,4)
                    except:
                        pass


        if random == False:
            self.add_nodes_from(pregraph.nodes)
            self.add_edges_from(pregraph.edges)

        self.adjacency_matrix = asarray(nx.to_numpy_matrix(self))
        self.m = self.number_of_edges() * 2

        # Generate modularity matrix (B)
        B = []
        for i in range(len(self.adjacency_matrix)):
            B.append([])
            for j in range(len(self.adjacency_matrix)):
                B[i].append(0.)
        B = array(B)
        for i in range(len(self.adjacency_matrix)):
            for j in range(len(self.adjacency_matrix[i])):
                q = self.adjacency_matrix[i][j] - (sum(self.adjacency_matrix[i]) * sum(self.adjacency_matrix[j])) / (
                        2 * self.m)
                B[i][j] = q
        self.modularity_matrix = B

        self.eigenvalues = []
        self.p = 0
        self.k = 0
        self.groups = []
        self.number_of_groups = 0

        self.max_rank = max_rank

        self.pos = nx.spring_layout(self)

    def prep_divide(self, k, basic_group_vectors=False):
        for g in self.nodes:
            self.nodes[g]['group'] = 0

        # Initialization of some variables
        self.p = k  # lower bound of p
        self.k = k

        ## Generate p leading eigenvalues ##

        self.eigenvalues = numpy.sort(
            linalg.eig(self.modularity_matrix)[0])  # Get a list of all eigenvalues, sorted by value
        for j in range(len(self.eigenvalues)):
            if self.eigenvalues[-k] <= 0:
                # p is set to the index of the last positive eigenvalue, so long as it isn't greater than the requested max_rank
                if j > self.p and j <= self.max_rank:
                    self.p = j + 1
                    break
                elif j > self.p:
                    self.p = self.max_rank
                    break

        indices = argpartition(linalg.eig(self.modularity_matrix)[0], -self.p)[-self.p:]
        self.eigenvalues = [linalg.eig(self.modularity_matrix)[0][k] for k in indices]
        vectors = [linalg.eig(self.modularity_matrix)[1][:, k] for k in indices]

        for j in self.eigenvalues:
            if j < 0:
                logging.info("Eigenvalue below zero: " + str(j))

        # Generate vertex vectors
        for node in self.nodes:
            self.nodes[node]['vector'] = array([sqrt(self.eigenvalues[l]) * vectors[l][node] for l in range(self.p)])

        ## Generate group vectors

        # Uses the trivial bases vectors [1,0,...], [0,1,0,...], ...
        if basic_group_vectors == True:
            self.group_vectors = [zeros(self.p) for s in range(self.k)]
            for s in range(self.k):
                self.group_vectors[s][s] = 1

        # Takes k random vertex vectors and uses them as initial group vectors
        if basic_group_vectors == False:
            import random
            self.group_vectors = random.sample([self.nodes[node]['vector'] for node in self.nodes], k)

        # For saving pictures of the iterative process
        self.saved_pictures = 0

        # Initializing some group variables
        self.groups = []
        self.number_of_groups = 0

    def update_number_of_groups(self):
        for i in self.nodes:
            if self.nodes[i]['group'] not in self.groups:
                self.groups.append(self.nodes[i]['group'])
        """
        for s in range(len(self.group_vectors)):
            try:
                if self.group_vectors[s] == 0:
                    print("Deleting group " + str(s))
                    del (self.group_vectors[s])
                    self.groups.remove(s)
                    self.k = len(self.group_vectors)
                    self.groups.sort()
                    for t in self.groups:
                        if t - 1 not in self.groups:
                            t = t - 1

            except:
                pass
        """

        self.number_of_groups = len(self.groups)

    def calculate_Q(self):
        return (1 / (2 * self.m)) * sum(
            [absolute(numpy.sum(
                [self.nodes[i]['vector'] for i in range(len(self.nodes)) if self.nodes[i]['group'] == s]
            )) ** 2 for s in range(self.k)]
        )

    def compute_inner_products(self):
        moves = []
        for i in self.nodes:
            current = {'group': self.nodes[i]['group'],
                       'value': transpose(self.group_vectors[self.nodes[i]['group']] - self.nodes[i]['vector']) @
                                self.nodes[i]['vector']}

            for s in range(self.k):
                if (self.group_vectors[s] == 0).all():
                    continue
                if self.nodes[i]['group'] == s:
                    test = transpose(self.group_vectors[s] - self.nodes[i]['vector']) @ self.nodes[i]['vector']
                    if test > current['value']:
                        print(
                            "Node " + str(i) + " moved from group " + str(self.nodes[i]['group']) + " to group " + str(
                                s))
                        current['group'] = s
                        current['value'] = test
                else:
                    test = transpose(self.group_vectors[s]) @ self.nodes[i]['vector']
                    if test > current['value']:
                        logging.info(
                            "Node " + str(i) + " moved from group " + str(self.nodes[i]['group']) + " to group " + str(
                                s))
                        logging.info("Distance from " + str(i) + " to " +str(self.nodes[i]['group']) + " is " + str(current['value']) + ", while distance to " + str(s) + " is " + str(test))
                        current['group'] = s
                        current['value'] = test
            moves.append((self.nodes[i]['group'],current['group']))
            self.nodes[i]['group'] = current['group']
        return moves

    def update_group_vectors(self):
        old = copy.deepcopy(self.group_vectors)
        for s in range(self.k):
            self.group_vectors[s] = sum(
                [self.nodes[i]['vector'] for i in range(len(self.nodes)) if self.nodes[i]['group'] == s])
            try:
                # Replacing group vectors with no members with a zero vector
                if self.group_vectors[s] == 0:
                    self.group_vectors[s] = zeros(self.p)

            except:
                pass
        self.update_number_of_groups()

        # If there's no change in group vectors, then return false
        if numpy.array_equal(old, self.group_vectors):
            change = False
            # Check if there are stranded nodes with only one edge, where the other node is in another group, and if so move them to the other node's group
            for i in self.nodes:
                if len(self.edges(i)) == 1 and self.nodes[i]['group'] != self.nodes[list(self.edges(i))[0][1]]['group']:
                    logging.info("One Edge - Node " + str(i) + " moved from group " + str(
                        self.nodes[i]['group']) + " to group " + str(self.nodes[list(self.edges(i))[0][1]]['group']))
                    self.nodes[i]['group'] = self.nodes[list(self.edges(i))[0][1]]['group']
                    change = True
            if change == True:
                return self.update_group_vectors()
            else:
                return change
            #return False <-- used if you decide to remove the code moving nodes with one edge to their neighbour's group

        else:
            return True

    def divide(self, k, basic_group_vectors=False):
        self.prep_divide(k, basic_group_vectors=basic_group_vectors)
        self.compute_inner_products()
        moves = []
        while self.update_group_vectors():
            logging.info("new iteration")
            self.show()
            self.draw_vectors()
            moves.append(self.compute_inner_products())
            try:
                if moves[-1] in [moves[-2],moves[-3],moves[-4],moves[-5]]:
                    break
            except:
                pass
        q = "Q = " + str(self.calculate_Q())
        self.show(title=q)
        logging.info(q)
        for i in self.nodes:
            distances = []
            for s in range(self.k):
                if self.nodes[i]['group'] == s:
                    distances.append((s, str(transpose(
                        self.group_vectors[s] - self.nodes[i]['vector']) @ self.nodes[i]['vector'])))
                else:
                    distances.append((s, str(
                        transpose(self.group_vectors[s]) @ self.nodes[i]['vector'])))

            logging.info("distances from node " + str(i) + " is " + str(distances))
        return self.calculate_Q()

    def multiple_divide(self, max_k, basic_group_vectors=False):
        Q_values = []
        for i in range(2, max_k + 1):
            # For the current amount of communities (i), try dividing three times and pick the best result in terms of Q
            #Q_values.append((i, max([self.divide(i,basic_group_vectors),self.divide(i, basic_group_vectors),self.divide(i, basic_group_vectors)])))

            # Just do one division
            Q_values.append((i, self.divide(i, basic_group_vectors)))

        logging.info("Q values: " + str(Q_values))

    def show(self,title=None):
        from coloring import create_colors
        color_map = []
        colors = create_colors(self.number_of_nodes())
        dic = {list(set([self.nodes[node]['group'] for node in self.nodes]))[i]: range(len(self.nodes))[i] for i in
               range(len(set([self.nodes[node]['group'] for node in self.nodes])))}
        # logging.info(dic)
        for node in self:
            color_map.append(colors[dic[self.nodes[node]['group']]])
        plt.ioff()
        plt.figure(self.saved_pictures, figsize=(12, 12))
        nx.draw(self, node_color=color_map, with_labels=True, pos=self.pos)
        nx.draw_networkx_edge_labels(self,self.pos,edge_labels={(u,v):self[u][v]['weight'] for u,v in self.edges})
        import matplotlib.patches as mpatches
        plt.legend(handles=[mpatches.Patch(color=colors[dic[group]], label=str(group)) for group in
                            list(set([self.nodes[node]['group'] for node in self.nodes]))])
        plt.suptitle(title, fontsize=14, fontweight='bold')

        # Add the weights of edges
        #labels = nx.get_edge_attributes(self, 'weight')
        #nx.draw_networkx_edge_labels(self, nx.get_node_attributes(self,'pos'), edge_labels=labels)

        plt.savefig("graph_division_" + str(self.k) + "_" + str(self.saved_pictures) + ".png")
        self.saved_pictures += 1
        plt.close("all")

    def draw_vectors(self):
        from vectors import plot_vectors
        from coloring import create_colors
        color_map = []
        color_map2 = []
        colors = create_colors(self.number_of_nodes())
        dic = {list(set([self.nodes[node]['group'] for node in self.nodes]))[i]: range(len(self.nodes))[i] for i in
               range(len(set([self.nodes[node]['group'] for node in self.nodes])))}
        # logging.info(dic)
        for node in self:
            color_map.append(colors[dic[self.nodes[node]['group']]])
        #for i in range(1,len(self.group_vectors)):
        #    color_map2.append(colors[dic[i]])
        plot_vectors(k=self.k,
                     n=len(self.nodes),
                     group_vectors=[vector.real for vector in self.group_vectors],
                     vertex_vectors=[self.nodes[i]['vector'].real for i in self.nodes],
                     number=str(self.k) + "_" + str(self.saved_pictures),
                     color_map_vertex_vectors=color_map,
                     color_map_group_vectors=color_map2)

@profile
class graph(nx.Graph):
    def __init__(self, adjacency_matrix, max_rank=100):
        super().__init__(adjacency_matrix)

        self.adjacency_matrix = adjacency_matrix
        self.m = self.number_of_edges() * 2

        """
        # Generate modularity matrix (B)
        self.modularity_matrix = numpy.zeros(self.adjacency_matrix.shape).astype(int)
        for i in range(len(self.adjacency_matrix)):
            for j in range(len(self.adjacency_matrix[i])):
                self.modularity_matrix[i][j] = self.adjacency_matrix[i][j] - (sum(self.adjacency_matrix[i]) * sum(self.adjacency_matrix[j])) / (
                        2 * self.m)

        self.eigenvalues = []
        self.p = 0
        self.k = 0
        self.groups = []
        self.number_of_groups = 0

        self.max_rank = max_rank

        self.pos = nx.spring_layout(self)
        """

    def prep_divide(self, k, basic_group_vectors=False):
        for g in self.nodes:
            self.nodes[g]['group'] = 0

        # Initialization of some variables
        self.p = k  # lower bound of p
        self.k = k

        ## Generate p leading eigenvalues ##

        self.eigenvalues = numpy.sort(
            linalg.eig(self.modularity_matrix)[0])  # Get a list of all eigenvalues, sorted by value
        for j in range(len(self.eigenvalues)):
            if self.eigenvalues[-k] <= 0:
                # p is set to the index of the last positive eigenvalue, so long as it isn't greater than the requested max_rank
                if j > self.p and j <= self.max_rank:
                    self.p = j + 1
                    break
                elif j > self.p:
                    self.p = self.max_rank
                    break

        indices = argpartition(linalg.eig(self.modularity_matrix)[0], -self.p)[-self.p:]
        self.eigenvalues = [linalg.eig(self.modularity_matrix)[0][k] for k in indices]
        vectors = [linalg.eig(self.modularity_matrix)[1][:, k] for k in indices]

        for j in self.eigenvalues:
            if j < 0:
                logging.info("Eigenvalue below zero: " + str(j))

        # Generate vertex vectors
        for node in self.nodes:
            self.nodes[node]['vector'] = array([sqrt(self.eigenvalues[l]) * vectors[l][node] for l in range(self.p)])

        ## Generate group vectors

        # Uses the trivial bases vectors [1,0,...], [0,1,0,...], ...
        if basic_group_vectors == True:
            self.group_vectors = [zeros(self.p) for s in range(self.k)]
            for s in range(self.k):
                self.group_vectors[s][s] = 1

        # Takes k random vertex vectors and uses them as initial group vectors
        if basic_group_vectors == False:
            import random
            self.group_vectors = random.sample([self.nodes[node]['vector'] for node in self.nodes], k)

        # For saving pictures of the iterative process
        self.saved_pictures = 0

        # Initializing some group variables
        self.groups = []
        self.number_of_groups = 0

    def update_number_of_groups(self):
        for i in self.nodes:
            if self.nodes[i]['group'] not in self.groups:
                self.groups.append(self.nodes[i]['group'])
        """
        for s in range(len(self.group_vectors)):
            try:
                if self.group_vectors[s] == 0:
                    print("Deleting group " + str(s))
                    del (self.group_vectors[s])
                    self.groups.remove(s)
                    self.k = len(self.group_vectors)
                    self.groups.sort()
                    for t in self.groups:
                        if t - 1 not in self.groups:
                            t = t - 1

            except:
                pass
        """

        self.number_of_groups = len(self.groups)

    def calculate_Q(self):
        return (1 / (2 * self.m)) * sum(
            [absolute(numpy.sum(
                [self.nodes[i]['vector'] for i in range(len(self.nodes)) if self.nodes[i]['group'] == s]
            )) ** 2 for s in range(self.k)]
        )

    def compute_inner_products(self):
        moves = []
        for i in self.nodes:
            current = {'group': self.nodes[i]['group'],
                       'value': transpose(self.group_vectors[self.nodes[i]['group']] - self.nodes[i]['vector']) @
                                self.nodes[i]['vector']}

            for s in range(self.k):
                if (self.group_vectors[s] == 0).all():
                    continue
                if self.nodes[i]['group'] == s:
                    test = transpose(self.group_vectors[s] - self.nodes[i]['vector']) @ self.nodes[i]['vector']
                    if test > current['value']:
                        print(
                            "Node " + str(i) + " moved from group " + str(self.nodes[i]['group']) + " to group " + str(
                                s))
                        current['group'] = s
                        current['value'] = test
                else:
                    test = transpose(self.group_vectors[s]) @ self.nodes[i]['vector']
                    if test > current['value']:
                        logging.info(
                            "Node " + str(i) + " moved from group " + str(self.nodes[i]['group']) + " to group " + str(
                                s))
                        logging.info("Distance from " + str(i) + " to " +str(self.nodes[i]['group']) + " is " + str(current['value']) + ", while distance to " + str(s) + " is " + str(test))
                        current['group'] = s
                        current['value'] = test
            moves.append((self.nodes[i]['group'],current['group']))
            self.nodes[i]['group'] = current['group']
        return moves

    def update_group_vectors(self):
        old = copy.deepcopy(self.group_vectors)
        for s in range(self.k):
            self.group_vectors[s] = sum(
                [self.nodes[i]['vector'] for i in range(len(self.nodes)) if self.nodes[i]['group'] == s])
            try:
                # Replacing group vectors with no members with a zero vector
                if self.group_vectors[s] == 0:
                    self.group_vectors[s] = zeros(self.p)

            except:
                pass
        self.update_number_of_groups()

        # If there's no change in group vectors, then return false
        if numpy.array_equal(old, self.group_vectors):
            change = False
            # Check if there are stranded nodes with only one edge, where the other node is in another group, and if so move them to the other node's group
            for i in self.nodes:
                if len(self.edges(i)) == 1 and self.nodes[i]['group'] != self.nodes[list(self.edges(i))[0][1]]['group']:
                    logging.info("One Edge - Node " + str(i) + " moved from group " + str(
                        self.nodes[i]['group']) + " to group " + str(self.nodes[list(self.edges(i))[0][1]]['group']))
                    self.nodes[i]['group'] = self.nodes[list(self.edges(i))[0][1]]['group']
                    change = True
            if change == True:
                return self.update_group_vectors()
            else:
                return change
            #return False <-- used if you decide to remove the code moving nodes with one edge to their neighbour's group

        else:
            return True

    def divide(self, k, basic_group_vectors=False):
        self.prep_divide(k, basic_group_vectors=basic_group_vectors)
        self.compute_inner_products()
        moves = []
        while self.update_group_vectors():
            logging.info("new iteration")
            self.show()
            self.draw_vectors()
            moves.append(self.compute_inner_products())
            try:
                if moves[-1] in [moves[-2],moves[-3],moves[-4],moves[-5]]:
                    break
            except:
                pass
        q = "Q = " + str(self.calculate_Q())
        self.show(title=q)
        logging.info(q)
        for i in self.nodes:
            distances = []
            for s in range(self.k):
                if self.nodes[i]['group'] == s:
                    distances.append((s, str(transpose(
                        self.group_vectors[s] - self.nodes[i]['vector']) @ self.nodes[i]['vector'])))
                else:
                    distances.append((s, str(
                        transpose(self.group_vectors[s]) @ self.nodes[i]['vector'])))

            logging.info("distances from node " + str(i) + " is " + str(distances))
        return self.calculate_Q()

    def multiple_divide(self, max_k, basic_group_vectors=False):
        Q_values = []
        for i in range(2, max_k + 1):
            # For the current amount of communities (i), try dividing three times and pick the best result in terms of Q
            #Q_values.append((i, max([self.divide(i,basic_group_vectors),self.divide(i, basic_group_vectors),self.divide(i, basic_group_vectors)])))

            # Just do one division
            Q_values.append((i, self.divide(i, basic_group_vectors)))

        logging.info("Q values: " + str(Q_values))

    def show(self,title=None):
        from coloring import create_colors
        color_map = []
        colors = create_colors(self.number_of_nodes())
        dic = {list(set([self.nodes[node]['group'] for node in self.nodes]))[i]: range(len(self.nodes))[i] for i in
               range(len(set([self.nodes[node]['group'] for node in self.nodes])))}
        # logging.info(dic)
        for node in self:
            color_map.append(colors[dic[self.nodes[node]['group']]])
        plt.ioff()
        plt.figure(self.saved_pictures, figsize=(12, 12))
        nx.draw(self, node_color=color_map, with_labels=True, pos=self.pos)
        nx.draw_networkx_edge_labels(self,self.pos,edge_labels={(u,v):self[u][v]['weight'] for u,v in self.edges})
        import matplotlib.patches as mpatches
        plt.legend(handles=[mpatches.Patch(color=colors[dic[group]], label=str(group)) for group in
                            list(set([self.nodes[node]['group'] for node in self.nodes]))])
        plt.suptitle(title, fontsize=14, fontweight='bold')

        # Add the weights of edges
        #labels = nx.get_edge_attributes(self, 'weight')
        #nx.draw_networkx_edge_labels(self, nx.get_node_attributes(self,'pos'), edge_labels=labels)

        plt.savefig("graph_division_" + str(self.k) + "_" + str(self.saved_pictures) + ".png")
        self.saved_pictures += 1
        plt.close("all")

    def draw_vectors(self):
        from vectors import plot_vectors
        from coloring import create_colors
        color_map = []
        color_map2 = []
        colors = create_colors(self.number_of_nodes())
        dic = {list(set([self.nodes[node]['group'] for node in self.nodes]))[i]: range(len(self.nodes))[i] for i in
               range(len(set([self.nodes[node]['group'] for node in self.nodes])))}
        # logging.info(dic)
        for node in self:
            color_map.append(colors[dic[self.nodes[node]['group']]])
        #for i in range(1,len(self.group_vectors)):
        #    color_map2.append(colors[dic[i]])
        plot_vectors(k=self.k,
                     n=len(self.nodes),
                     group_vectors=[vector.real for vector in self.group_vectors],
                     vertex_vectors=[self.nodes[i]['vector'].real for i in self.nodes],
                     number=str(self.k) + "_" + str(self.saved_pictures),
                     color_map_vertex_vectors=color_map,
                     color_map_group_vectors=color_map2)

"""
Algorithm:
1. Choose an initial set of group vectors R_s, one for each of the k communities
2. Compute the inner product transpose(R_s)r_i for all vertices i and all communities s
     OR
   transpose(R_s - r_i)r_i if vertex i is assigned to group s
3. Assign each vertex i to the community with which it has the most positive inner product
4. Update the group vectors R_s (calculate_R)
5. Repeat until 4 yields no change
"""


def test():
    b = nx.Graph()
    b.add_nodes_from(
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
         30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
         58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85,
         86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
         111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132,
         133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149])
    b.add_edges_from(
        [(0, 1), (0, 16), (0, 25), (0, 66), (1, 2), (1, 3), (1, 5), (1, 17), (1, 37), (1, 56), (1, 57), (1, 67),
         (1, 76), (1, 80), (1, 90), (1, 118), (1, 130), (2, 4), (2, 12), (2, 13), (2, 34), (2, 36), (2, 72), (2, 111),
         (3, 30), (3, 42), (3, 60), (3, 92), (3, 138), (5, 6), (5, 7), (5, 14), (5, 15), (5, 45), (5, 112), (5, 137),
         (6, 27), (6, 55), (6, 99), (7, 8), (7, 9), (7, 10), (7, 114), (7, 121), (7, 136), (9, 11), (9, 103), (10, 33),
         (10, 49), (10, 109), (11, 35), (11, 69), (11, 141), (12, 68), (12, 124), (12, 125), (13, 21), (13, 29),
         (13, 40), (13, 41), (13, 44), (13, 47), (13, 95), (13, 96), (13, 102), (13, 119), (13, 128), (13, 142),
         (14, 28), (14, 75), (14, 140), (15, 18), (15, 19), (15, 23), (15, 24), (15, 31), (15, 59), (15, 70), (15, 81),
         (15, 97), (15, 98), (15, 126), (15, 132), (15, 143), (18, 20), (18, 26), (21, 22), (21, 131), (23, 43),
         (23, 86), (24, 32), (25, 58), (25, 93), (25, 123), (26, 38), (26, 51), (26, 74), (26, 116), (26, 122),
         (27, 61), (28, 78), (29, 39), (32, 65), (32, 113), (33, 52), (33, 89), (34, 54), (34, 77), (34, 87), (36, 48),
         (36, 50), (36, 106), (36, 134), (36, 139), (37, 46), (39, 62), (39, 117), (44, 88), (45, 53), (45, 71),
         (45, 110), (50, 79), (50, 105), (51, 64), (52, 63), (52, 83), (52, 85), (54, 129), (56, 91), (57, 101),
         (61, 94), (62, 100), (68, 108), (70, 73), (72, 84), (76, 82), (94, 120), (96, 149), (100, 107), (102, 104),
         (103, 135), (104, 127), (112, 115), (114, 145), (116, 148), (117, 147), (120, 146), (125, 133), (142, 144)])
    g = graph(pregraph=b, random=False)
    g.divide(12)
    return g


@profile
def from_numpy_matrix(A, parallel_edges=False, create_using=None):
    """Returns a graph from numpy matrix.

    The numpy matrix is interpreted as an adjacency matrix for the graph.

    Parameters
    ----------
    A : numpy matrix
        An adjacency matrix representation of a graph

    parallel_edges : Boolean
        If True, `create_using` is a multigraph, and `A` is an
        integer matrix, then entry *(i, j)* in the matrix is interpreted as the
        number of parallel edges joining vertices *i* and *j* in the graph.
        If False, then the entries in the adjacency matrix are interpreted as
        the weight of a single edge joining the vertices.

    create_using : NetworkX graph constructor, optional (default=nx.Graph)
       Graph type to create. If graph instance, then cleared before populated.

    Notes
    -----
    For directed graphs, explicitly mention create_using=nx.DiGraph,
    and entry i,j of A corresponds to an edge from i to j.

    If `create_using` is :class:`networkx.MultiGraph` or
    :class:`networkx.MultiDiGraph`, `parallel_edges` is True, and the
    entries of `A` are of type :class:`int`, then this function returns a
    multigraph (constructed from `create_using`) with parallel edges.

    If `create_using` indicates an undirected multigraph, then only the edges
    indicated by the upper triangle of the matrix `A` will be added to the
    graph.

    If the numpy matrix has a single data type for each matrix entry it
    will be converted to an appropriate Python data type.

    If the numpy matrix has a user-specified compound data type the names
    of the data fields will be used as attribute keys in the resulting
    NetworkX graph.

    See Also
    --------
    to_numpy_matrix, to_numpy_recarray

    Examples
    --------
    Simple integer weights on edges:

    >>> import numpy as np
    >>> A = np.array([[1, 1], [2, 1]])
    >>> G = nx.from_numpy_matrix(A)

    If `create_using` indicates a multigraph and the matrix has only integer
    entries and `parallel_edges` is False, then the entries will be treated
    as weights for edges joining the nodes (without creating parallel edges):

    >>> A = np.array([[1, 1], [1, 2]])
    >>> G = nx.from_numpy_matrix(A, create_using=nx.MultiGraph)
    >>> G[1][1]
    AtlasView({0: {'weight': 2}})

    If `create_using` indicates a multigraph and the matrix has only integer
    entries and `parallel_edges` is True, then the entries will be treated
    as the number of parallel edges joining those two vertices:

    >>> A = np.array([[1, 1], [1, 2]])
    >>> temp = nx.MultiGraph()
    >>> G = nx.from_numpy_matrix(A, parallel_edges=True, create_using=temp)
    >>> G[1][1]
    AtlasView({0: {'weight': 1}, 1: {'weight': 1}})

    User defined compound data type on edges:

    >>> dt = [("weight", float), ("cost", int)]
    >>> A = np.array([[(1.0, 2)]], dtype=dt)
    >>> G = nx.from_numpy_matrix(A)
    >>> list(G.edges())
    [(0, 0)]
    >>> G[0][0]["cost"]
    2
    >>> G[0][0]["weight"]
    1.0

    """
    # This should never fail if you have created a numpy matrix with numpy...
    import numpy as np
    import itertools

    kind_to_python_type = {
        "f": float,
        "i": int,
        "u": int,
        "b": bool,
        "c": complex,
        "S": str,
        "V": "void",
    }
    kind_to_python_type["U"] = str
    G = nx.empty_graph(0, create_using)
    n, m = A.shape
    if n != m:
        raise nx.NetworkXError(f"Adjacency matrix not square: nx,ny={A.shape}")
    dt = A.dtype
    try:
        python_type = kind_to_python_type[dt.kind]
    except Exception as e:
        raise TypeError(f"Unknown numpy data type: {dt}") from e

    # Make sure we get even the isolated nodes of the graph.
    G.add_nodes_from(range(n))
    # Get a list of all the entries in the matrix with nonzero entries. These
    # coordinates become edges in the graph. (convert to int from np.int64)
    edges = ((int(e[0]), int(e[1])) for e in zip(*np.asarray(A).nonzero()))
    # handle numpy constructed data type
    if python_type == "void":
        # Sort the fields by their offset, then by dtype, then by name.
        fields = sorted(
            (offset, dtype, name) for name, (dtype, offset) in A.dtype.fields.items()
        )
        triples = (
            (
                u,
                v,
                {
                    name: kind_to_python_type[dtype.kind](val)
                    for (_, dtype, name), val in zip(fields, A[u, v])
                },
            )
            for u, v in edges
        )
    # If the entries in the adjacency matrix are integers, the graph is a
    # multigraph, and parallel_edges is True, then create parallel edges, each
    # with weight 1, for each entry in the adjacency matrix. Otherwise, create
    # one edge for each positive entry in the adjacency matrix and set the
    # weight of that edge to be the entry in the matrix.
    elif python_type is int and G.is_multigraph() and parallel_edges:
        chain = itertools.chain.from_iterable
        # The following line is equivalent to:
        #
        #     for (u, v) in edges:
        #         for d in range(A[u, v]):
        #             G.add_edge(u, v, weight=1)
        #
        triples = chain(
            ((u, v, {"weight": 1}) for d in range(A[u, v])) for (u, v) in edges
        )
    else:  # basic data type
        triples = ((u, v, dict(weight=python_type(A[u, v]))) for u, v in edges)
    # If we are creating an undirected multigraph, only add the edges from the
    # upper triangle of the matrix. Otherwise, add all the edges. This relies
    # on the fact that the vertices created in the
    # `_generated_weighted_edges()` function are actually the row/column
    # indices for the matrix `A`.
    #
    # Without this check, we run into a problem where each edge is added twice
    # when `G.add_edges_from()` is invoked below.
    if G.is_multigraph() and not G.is_directed():
        triples = ((u, v, d) for u, v, d in triples if u <= v)
    G.add_edges_from(triples)
    return G



@profile
def create_graph_from_matrix(filename):
    import csv
    file = open(filename,'r')
    csv_reader = csv.reader(file)
    lists_from_csv = []
    for row in csv_reader:
        lists_from_csv.append(row)

    del(lists_from_csv[0])

    for row in lists_from_csv:
        del(row[0])

    #a = numpy.array(lists_from_csv)
    #g = from_numpy_matrix(a)

    return(graph(numpy.array(lists_from_csv)))


#    g = from_numpy_matrix(numpy.array(lists_from_csv)[:1000,:1000])
    #return(graph(pregraph=g,random=False))

if __name__ == '__main__':
    create_graph_from_matrix('randomflow.csv')
