# Modularity calculator

The program takes a directed graph and calculates its modularity score Q. It produces pictures of the process of subdividing the graph and the vector space used to calculate it.

The program is an implementation of the algorith described in this paper: https://sci-hub.st/10.1103/physreve.92.052808.

The basic idea is to 

1. Choose an initial set of group vectors $`R_s`$, one for each of the k communities created using Newman's community partitioning algorithm.
2. Compute the inner product $`(R_s)^T * r_i`$ for all vertices i and all communities s,
    OR the inner product $`(R_s - r_i)^T * r_i`$ if vertex i is in group s.
3. Assign each vertex i to the community with which its inner product is greatest.
4. Update the group vectors $`R_s`$.
5. Repeat until 4 yields no change.
