from numpy import array
from numpy import nan_to_num
from numpy import mean
from numpy import cov
from numpy.linalg import eig
import numpy
import matplotlib.pyplot as plt
import logging

logging.basicConfig(filename='log.log', level=logging.INFO, filemode="w")

def normalize(vectors):
    return nan_to_num(array([row/numpy.sqrt(sum([i**2 for i in row])) for row in vectors]))


def plot_vectors(k,n,group_vectors,vertex_vectors,number,color_map_vertex_vectors,color_map_group_vectors):

    # Principal Component Analysis
    from numpy import array
    from sklearn.decomposition import PCA
    from sklearn.preprocessing import StandardScaler

    ### Vertex vectors ###

    # define a matrix
    A = array([vector for vector in vertex_vectors])
    print(A)
    # create the PCA instance
    pca = PCA(2)
    # fit on data
    pca.fit(A)
    # access values and vectors
    # print(pca.components_)
    # print(pca.explained_variance_)
    logging.info('Explained variation per principal component for vertex vectors: {}'.format(pca.explained_variance_ratio_))
    log1 = pca.explained_variance_ratio_
    # transform data
    B = pca.transform(A)
    print(B)

    # Normalize the vectors
    B = normalize(B)

    origin = array([numpy.zeros(n), numpy.zeros(n)])  # origin point

    plt.quiver(*origin, B[:, 0], B[:, 1], color=color_map_vertex_vectors,scale=4)

    ### Group vectors ###

    # define a matrix
    A = array([vector for vector in group_vectors])
    print(A)
    # create the PCA instance
    pca = PCA(2)
    # fit on data
    pca.fit(A)
    # access values and vectors
    # print(pca.components_)
    # print(pca.explained_variance_)
    logging.info('Explained variation per principal component for group vectors: {}'.format(pca.explained_variance_ratio_))
    log2 = pca.explained_variance_ratio_
    # transform data
    B = pca.transform(A)
    print(B)

    # Normalize the vectors
    B = normalize(B)

    origin = array([numpy.zeros(k), numpy.zeros(k)])  # origin point

    #from coloring import create_colors
    #colors = create_colors(len(group_vectors))

    #plt.quiver(*origin, B[:, 0], B[:, 1], color=color_map_group_vectors, scale=2) <--- with coloring
    plt.quiver(*origin, B[:, 0], B[:, 1],scale=4)

    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    plt.axis([-1,1,-1,1])
    plt.suptitle("Information loss:", fontsize=13)
    plt.title("Vertex vectors - " + "{:.0%}". format(1-sum(log1)) + ", Group vectors - " + "{:.0%}". format(1-sum(log2)), fontsize=11)

    plt.savefig("vectors"+str(number)+".png")
    #plt.show()